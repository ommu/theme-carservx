theme-carservx
=============
Carservx - Car Service and Car Repair


Installation
------------
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist ommu/carservx "dev-master"
```

 or
```
 "ommu/carservx": "dev-master"
```

to the require section of your composer.json.


Preview
------------
https://theme.ommu.co/carservx-site
