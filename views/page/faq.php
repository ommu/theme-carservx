<?php
/**
 * @var $this app\components\View
 * @var $this themes\carservx\controllers\PageController
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 29 July 2019, 19:35 WIB
 * @link https://bitbucket.org/ommu/theme-carservx
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$themeAsset = \themes\carservx\assets\ThemePluginAsset::register($this);
?>

<div class="section-full p-t80 p-b50">
	<div class="container">
		<div class="row">
			<div class="col-md-4 m-b30">
				<div class="about4-section m-b30 bg-gray p-a20">
					<div class="about4-content">
						<h2>GET 10% OFF TODAY</h2>
						<p>A Completely Safe and Advanced Cleaning Solution for both Petrol and Diesel Engines</p>
						<a href="#" class="site-button site-btn-effect">Read More</a>
					</div>
				</div>
				<div class="service_hour">
					<h4>Service Hour</h4>
					<ul class="list-unstyled">
						<li>
							<span>Monday</span>
							<span>7.00 - 16.30</span>
						</li>
						<li>
							<span>Tuesday</span>
							<span>7.00 - 16.30</span>
						</li>
						<li>
							<span>Wednesday</span>
							<span>7.00 - 16.30</span>
						</li>
						<li>
							<span>Thursday</span>
							<span>7.00 - 16.30</span>
						</li>

						<li>
							<span>Saturday</span>
							<span>7.00 - 16.30</span>
						</li>

						<li>
							<span>Sunday</span>
							<span>7.00 - 16.30</span>
						</li>
					</ul>
				</div>

			</div>

			<div class="col-md-8">
				<div class="wt-box m-b80 faq-quetion">
					<h2>General Questions</h2>
					<div class="wt-accordion" id="accordion5">

						<div class="panel wt-panel">
							<div class="acod-head acc-actives">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseGeneral1" data-parent="#accordion5" >
										How long will it take to repair my vehicle? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseGeneral1" class="acod-body collapse in">
								<div class="acod-content p-a15">
									It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.web design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseGeneral2" class="collapsed" data-parent="#accordion5">
									Why does my car need maintenance if it's running fine? 
									<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseGeneral2" class="acod-body collapse">
								<div class="acod-content p-a15">Graphic design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseGeneral3" class="collapsed" data-parent="#accordion5">
										Do you service and repair all makes and models of vehicles? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseGeneral3" class="acod-body collapse">
								<div class="acod-content p-a15">Developement lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseGeneral4" class="collapsed" data-parent="#accordion5">
										Can i Modify Once my request once its submitted? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseGeneral4" class="acod-body collapse">
								<div class="acod-content p-a15">Graphic design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseGeneral5" class="collapsed" data-parent="#accordion5">
									How long will it take to repair my vehicle?
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseGeneral5" class="acod-body collapse">
								<div class="acod-content p-a15">Developement lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

					</div>
				</div>

				<div class="wt-box faq-quetion">
					<h2>About Services</h2>
					<div class="wt-accordion" id="accordion3">

						<div class="panel wt-panel">
							<div class="acod-head acc-actives">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseAbout1" data-parent="#accordion3" >
										Do you service and repair all makes and models of vehicles? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseAbout1" class="acod-body collapse in">
								<div class="acod-content p-a15">
									It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.web design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseAbout2" class="collapsed" data-parent="#accordion3">
									How often should I rotate my tires? 
									<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseAbout2" class="acod-body collapse">
								<div class="acod-content p-a15">Graphic design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseAbout3" class="collapsed" data-parent="#accordion3">
										What will happen if I don't have the repairs done right now? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseAbout3" class="acod-body collapse">
								<div class="acod-content p-a15">Developement lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>

						<div class="panel wt-panel">
							<div class="acod-head">
								<h4 class="acod-title">
									<a data-toggle="collapse" href="#collapseAbout4" class="collapsed" data-parent="#accordion3">
										If you don't have a part in stock, how long will it take to order? 
										<span class="indicator"><i class="fa fa-plus"></i></span>
									</a>
								</h4>
							</div>
							<div id="collapseAbout4" class="acod-body collapse">
								<div class="acod-content p-a15">Graphic design lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised sheets containing Lorem Ipsum passagese.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php //begin.faq
echo \themes\carservx\components\HomeFaqs::widget(); ?>